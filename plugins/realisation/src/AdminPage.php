<?php

namespace App\Realisation;

Class AdminPage{

    /**
     * Register WP Actions
     */
    public function register()
    {
        add_action( 'admin_menu', [$this , 'admin_page_realisation'] );
    }

    /**
     * Get the template for the Dashboard plugin
     */
    public function admin_dashboard()
    {
        return require_once(PLUGIN_PATH."/templates/settings.php");
    }

    /**
     * Register the Dashboard Menu for the plugin with no subpages
     */
    public function admin_page_realisation() {
        add_menu_page(
            'Realisation settings',
            'Realis.. settings',
            'manage_options',
            'dashboard', // slug
            [ $this , 'admin_dashboard' ], // Callback
            'dashicons-marker', // Icon
            6,
        );
    }

    

}