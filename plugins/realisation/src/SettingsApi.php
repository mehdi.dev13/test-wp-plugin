<?php

namespace App\Realisation;

Class SettingsApi{

    /**
     * Action WP
     */
    public function register()
    {
        add_action( 'admin_init', [$this , 'realisation_plugin_settings_init'] );
    }

    /**
     * Register the settings Api for the number realisatio per page.
     */
    public function realisation_plugin_settings_init() {
        register_setting('realisation', 'realisation_post_per_page');
    
        // register a new section in the "realisation" page.
        add_settings_section(
            'realisation_settings_section',
            'Realisation per page :', [$this, 'realisation_settings_section_callback'],
            'realisation'
        );
    
        // register a new field in the "realisation_settings_section" section, inside the "realisation" page
        add_settings_field(
            'realisation_settings_field',
            'Number realisation :', [$this, 'realisation_settings_field_callback'],
            'realisation',
            'realisation_settings_section'
        );
    }

    
    /**
     * Callback for the settings Api (input label).
     */
    public function realisation_settings_section_callback() {
        echo '<p>Set the number of realisations to show per page.</p>';
    }

    /**
     * Callback for the settings Api (Input).
     */
    public function realisation_settings_field_callback() {
        // get the value of the setting we've registered with register_setting().
        $setting = get_option('realisation_post_per_page');
        // output the field.
        ?>
        <input type="text" name="realisation_post_per_page" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
        <?php
    }
}