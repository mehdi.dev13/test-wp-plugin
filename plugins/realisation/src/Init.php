<?php 

/**
 *@package Realisation
*/

namespace App\Realisation;

// This class will instanciate all register functions on 'get_classes' classes
final class Init
{
    /**
     * Get all registred Class
     * @return class
    */
    public static function get_classes()
    {
        return [
            Activate::class,
            AdminPage::class,
            SettingsApi::class
        ];
    }

    public static function register_classes()
    {
        foreach (self::get_classes() as $class) {
            $service = self::instanciate($class);
            if(method_exists($service,'register')){
                $service->register();
            }
            
        }
    }

    private static function instanciate($class)
    {
        $instance = new $class;
        return $instance;
    }


    
}