<?php

/**
 *@package Realisation
*/

namespace App\Realisation;


Class Activate{
    /**
     * Register Wp Actions
     */
    public function register()
    {
        // add a message on plugin activation
        add_action( 'admin_notices', [$this, 'activation_admin_notice']);
        add_action( 'init', [$this , 'create_realisation_post_type'] );
    }
    
    /**
     * Add Notice on activation
     */
    public static function activate()
    {
        // Store this transient for 5 sec on options database.
        set_transient( 'realisation-plugin-activation', true, 5 );
    }    

    /**
     * Register Custom post Type (Realisation)
     */
    public function create_realisation_post_type() {
        $labels = array(
            'name' => __( 'Réalisations' ),
            'singular_name' => __( 'Réalisation' ),
            'add_new' => __( 'Ajouter une réalisation' ),
            'add_new_item' => __( 'Ajouter une nouvelle réalisation' ),
            'edit_item' => __( 'Modifier la réalisation' ),
            'new_item' => __( 'Nouvelle réalisation' ),
            'view_item' => __( 'Voir la réalisation' ),
            'search_items' => __( 'Rechercher une réalisation' ),
            'not_found' => __( 'Aucune réalisation trouvée' ),
            'not_found_in_trash' => __( 'Aucune réalisation trouvée dans la corbeille' )
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-portfolio',
            'supports' => array( 'title', 'editor', 'thumbnail' ) // At least this (as mentionned on the test)
        );
        
        register_post_type( 'realisation', $args );
        
        // Clear the permalinks after the post type has been registered.
        flush_rewrite_rules();  

    }

    /**
     * Show the notice 
     */
    public static function activation_admin_notice(){
        /* Check transient, if available display notice */
        if( get_transient( 'realisation-plugin-activation' ) ){
            ?>
            <div class="updated notice is-dismissible">
                <p>The pluign realisation for the technical test <strong>is activated</strong>.</p>
            </div>
            <?php
            // Delete it
            delete_transient( 'realisation-plugin-activation' );
        }
    }
}
