<?php

/**
 *  Realisation Plugin For technical test
 *
 * @package           Realisation
 * @author            Mehdi DAOUI
 * @copyright         2023
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Realisation
 * Plugin URI:        https://nn.c
 * Description:       the plugin create automaticaly a post type realisation on activation.
 * Version:           1.1.0
 * Requires at least: 6.0
 * Requires PHP:      8.0
 * Author:            Mehdi DAOUI
 * Author URI:        https://DM.com
 * Text Domain:       plugin-slug
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

if( !defined('ABSPATH') ){
    die;
}

if( file_exists( dirname(__FILE__).'/vendor/autoload.php')  ){
    require_once __DIR__.'/vendor/autoload.php';
}

use App\Realisation\Init;
use App\Realisation\Activate;
use App\Realisation\Deactivate;

if(class_exists(Init::class)){
    Init::register_classes();
}

Class Realisation{

    public function __construct()
    {
        register_activation_hook(__FILE__, [$this, 'activate_plugin']);
        register_deactivation_hook(__FILE__, [$this , 'deactivate_plugin']);
        define('PLUGIN_PATH',plugin_dir_path( __FILE__ ));
    }
    
    public function activate_plugin()
    {
        Activate::activate();
    }

    public function deactivate_plugin()
    {
        Deactivate::deactivate();
    }
}

new Realisation();



