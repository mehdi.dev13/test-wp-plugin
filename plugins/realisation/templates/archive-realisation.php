<?php get_header(); ?>

<main id="main" class="site-main">

	<header class="page-header">
		<h1 class="page-title"><?php post_type_archive_title(); ?></h1>
	</header><!-- .page-header -->

	<div class="posts-container">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-block' ); ?>>
					<?php if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php the_post_thumbnail( 'medium' ); ?>
						</a>
					<?php endif; ?>

					<header class="entry-header">
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_excerpt(); ?>
					</div><!-- .entry-content -->
				</article><!-- .post-block -->
			<?php endwhile; ?>
		<?php endif; ?>
	</div><!-- .posts-container -->

	<?php
		// Previous/next page navigation.
		the_posts_pagination(
			array(
				'prev_text' => __( 'Previous', 'textdomain' ),
				'next_text' => __( 'Next', 'textdomain' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'textdomain' ) . ' </span>',
			)
		);
	?>

</main><!-- .site-main -->

<?php get_footer(); ?>
