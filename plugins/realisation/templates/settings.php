<div class="wrap">
        <h1><?php _e( 'Réalisations Settings', 'realisation-plugin' ); ?></h1>

        <form method="post" action="options.php">
            <?php settings_fields( 'realisation' ); ?>
            <?php do_settings_sections( 'realisation' ); ?>
            <?php submit_button(); ?>
        </form>
    </div>